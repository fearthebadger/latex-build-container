This is a very simple LaTeX resume builder using Docker.

- Build the container

  - Docker

    ```bash 
    docker build -t resume .
    ```

  - Podman

    ```bash
    podman build -t resume .
    ```

- Create your `resume.tex` file in this directory.
- Build your resume (which creates a pdf file).

  - Docker

    ```bash
    docker run -v ${PWD}:/docs resume
    ```

  - Podman

    ```bash
    podman run -v ${PWD}:/docs:z resume
    ```
